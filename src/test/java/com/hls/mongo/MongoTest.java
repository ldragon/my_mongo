package com.hls.mongo;

import java.util.List;

import com.hls.mongo.Mongo;

/**
 * Unit test for simple App.
 */
public class MongoTest
{
    public static void main(String[] args) throws Exception {
        Mongo mongo = new Mongo();
        //create database
        //mongo.createDatabase("longshenga");
        
        //add user
//        Userinfo addUserinfo = new Userinfo("long","mima",30);
//        String uuid = mongo.insert("longsheng", addUserinfo);
//        System.out.println("unique id is "+uuid);
        
        //update user
        //uuid - ecf26c0e7de54e21a763c5ea50edfb07 sample
//        Userinfo updateUserinfo = new Userinfo("long-updated twice","mima",30);
//        boolean ret = mongo.update("longsheng", "ecf26c0e7de54e21a763c5ea50edfb07", updateUserinfo);
//        if(ret)
//        {
//            System.out.println("update succeed.");
//        }
        
        //getList
//        List<String> list = mongo.getList("longsheng");
//        System.out.println(list);
        
        //delete user
        //uuid - 7ac8d69d9a45450c9820ed2c789d5070 sample
//        boolean ret2 = mongo.delete("longsheng", "cf3d5f7c027e4084b1b1c4be1807fbae");
//        if(ret2)
//        {
//            System.out.println("delete succeed.");
//        }
        
        //drop database
        mongo.dropDatabase("longsheng1");
    }

}
