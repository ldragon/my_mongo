package com.hls.mongo.core;

import com.google.gson.Gson;

public class GsonHelper {
    public static String entityToJSONString(Object entity) {
        Gson gson = new Gson();
        return gson.toJson(entity);
    }
    public static Object JSONToEntity(String json,Class entityClass) {
        Gson gson = new Gson();
        Object res = gson.fromJson(json, entityClass);
        return res;
    }
}
