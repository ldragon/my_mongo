package com.hls.mongo.core;

import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.input.SAXBuilder;

public class ConfigReader {
    public static Map<String,String> configure(String path)
    {
        Map<String,String> map = new HashMap<String,String>();
        SAXBuilder builder = new SAXBuilder();
        Document document;
        try {
            document = builder.build(new File(path));
            Element root = document.getRootElement();
            List<Element> beans =  root.getChildren();
            for(Element item : beans)
            {
                //read the database file configuration
                if(item.getName().equals("dbstore"))
                {
                    map.put(item.getName(), item.getValue());
                }
            }
        }
        catch(Exception e)
        {
            e.printStackTrace();
            System.out.println("exception occured when reading the configuration file.");
        }
        return map;
    }
}
